package com.springernature.qatask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import searchPageObjects.SearchPageObjects;
import searchPageObjects.SearchResultsPageObjects;
import utils.ExecuteActions;



public class BaseClass {
	WebDriver driver;
	SearchPageObjects searchPage;
	SearchResultsPageObjects bsResults;
	ExecuteActions ea;
	public static final Logger log = LogManager.getLogger(BaseClass.class.getName());
	
	@BeforeClass
	public void beforeClassSetup() {
		System.setProperty("webdriver.gecko.driver", "/Users/prajakta.baurai/Documents/workspace/springerNatureTask/qatask/geckodriver");
	}
	
	@BeforeMethod
	public void setup(){
		driver = new FirefoxDriver();
		log.info("=====Browser Session Started=====", true);
		searchPage = new SearchPageObjects(driver);
		bsResults = new SearchResultsPageObjects(driver);
		ea = new ExecuteActions(driver);
		driver.get(utils.ConstantVariables.baseUrl);
	}
	
	
	@AfterMethod
	public void afterMethod(ITestResult testResult) throws InterruptedException {
		driver.quit();
		if (testResult.getStatus() == ITestResult.FAILURE) {
			log.info("=====" + testResult.getName() + ": FAIL =====", true);
		} else {
			log.info("=====" + testResult.getName() + ": PASS =====", true);
		}
		log.info("=====Browser Session End=====", true);
	}	
}
