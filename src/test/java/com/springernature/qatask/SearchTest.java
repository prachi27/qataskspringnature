package com.springernature.qatask;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SearchTest extends BaseClass {
	/*
	 * Valid Search TEST: Valid search results are displayed when user enters a
	 * valid Search term 
	 */
	
	@Test
	public void searchValidTerm_Test() {
		String searchStr = "economics";
		ea.enterSearchTerm(searchStr);
		String expectedResultsTitle = "Result(s) for \'" + searchStr + "\'";
		Assert.assertTrue(bsResults.searchResultsTitle().contains(expectedResultsTitle));
		log.info("Assertion PASSED for validating Search Results Title for the search String");
		ea.searchResultValidation(searchStr);
		ea.searchResultItemDetailsValidation();
		ea.clickNewSearchButton();
		Assert.assertTrue(bsResults.searchResultsTitle().contains("Result(s)"));
		log.info("Assertion PASSED for validating reset of Search Page");
	}

	/*
	 * Invalid Search - TEST: Appropriate error should be displayed when user
	 * enters invalid Search term 
	 */

	@Test
	public void searchInvalidTerm_Test() throws InterruptedException {
		String searchStr = "!£^&(&&((&^%";
		ea.enterSearchTerm(searchStr);
		String expectedResultsTitle = "0 Result(s) for \'" + searchStr + "\'";
		Assert.assertEquals(bsResults.searchResultsTitle(), expectedResultsTitle);
		log.info("Assertion PASSED for validating Search Results Title for the search String");
		ea.clickNewSearchButton();
		Assert.assertTrue(bsResults.searchResultsTitle().contains("Result(s)"));
		log.info("Assertion PASSED for validating reset of Search Page");
	}

}
