package searchPageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import utils.GenericMethods;
import utils.WaitTypes;

public class PageObjectsBaseClass {
	WebDriver driver;
	WaitTypes wt;
	GenericMethods gm;
	public static final Logger log = LogManager.getLogger(PageObjectsBaseClass.class.getName());
	
	public PageObjectsBaseClass(WebDriver driver) {
		this.driver =  driver;
		wt = new WaitTypes(driver);
		gm = new GenericMethods(driver);
		PageFactory.initElements(driver, this);
	}

}
