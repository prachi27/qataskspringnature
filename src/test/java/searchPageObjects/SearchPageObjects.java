package searchPageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchPageObjects extends PageObjectsBaseClass {
	
	@FindBy (id = "query")
	private static WebElement searchField;
	
	@FindBy (id = "search")
	private static WebElement searchButton;
	
	
	public SearchPageObjects(WebDriver driver) {
		super(driver);
	}
	
	public WebElement searchField() {
		WebElement element = wt.waitForElement(searchField);
		return element;
	}
	
	public void clickSearchField() {
		wt.waitForElement(searchField).click();
	}
	
	public void clickSearchButton() {
		wt.waitForElement(searchButton).click();
	}

}
