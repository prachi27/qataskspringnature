package searchPageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchResultsPageObjects extends PageObjectsBaseClass {
	
	@FindBy (css = ".number-of-search-results-and-search-terms")
	private static WebElement searchResultTitle;
	
	@FindBy (id = "global-search-new") 
	private static WebElement newSearchButton;
	
	public SearchResultsPageObjects(WebDriver driver) {
		super(driver);
	}
	
	public String searchResultsTitle() {
		String text = wt.waitForElement(searchResultTitle).getText();
		log.info("Search Results Title on Page: " + text);
		System.out.println("Search Results Title on Page: " + text);
		return text;
	}
	
	public WebElement getNewSearchButton() {
		WebElement element = wt.waitForElement(newSearchButton);
		return element;
	}
	
	public List<WebElement> resutlsList() {
		List<WebElement> itemList = gm.getItemListWait(By.cssSelector("#results-list>li"), 10);
		return itemList;
	}
	
	public List<WebElement> resutlTitles() {
		List<WebElement> itemList = gm.getItemListWait(By.cssSelector(".title"), 10);
		return itemList;
	}
	

}
