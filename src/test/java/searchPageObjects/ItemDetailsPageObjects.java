package searchPageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ItemDetailsPageObjects extends PageObjectsBaseClass {
	
	@FindBy(id = "title")
	private static WebElement pageTitle;

	public ItemDetailsPageObjects(WebDriver driver) {
		super(driver);
	}
	
	public String clickedItemPageTitle() {
		String text = wt.waitForElement(pageTitle).getText();
		log.info("Page title of clicked item on details page: " +text);
		return text;
	}

}
