package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitTypes {
	private WebDriver driver;
	public WaitTypes(WebDriver driver) {
		this.driver = driver;
	}

	
	public WebElement waitForElement(WebElement element) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			element = wait.until(ExpectedConditions.visibilityOf(element));
			System.out.println( element + "Element found");
		}
		catch (Exception e) {
			System.out.println( element + "Element not found");
		}
		return element;
		
	}
	

}
