package utils;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GenericMethods {
	WebDriver driver;
	WaitTypes waitTypes;
	
	public GenericMethods(WebDriver driver) {
		this.driver = driver;
		waitTypes = new WaitTypes(driver);
	}
	
	public WebElement waitForElement(By locator, int timeout) {
		WebElement element = null;
		try {
			WebDriverWait wait = new WebDriverWait(driver, timeout);
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		}
		catch (Exception e) {			
		}
		return element;
	}
	
	
	public List<WebElement> getElementListByLocator(By locator) {
		List<WebElement> elementList = driver.findElements(locator);
		return elementList;	 
	}
			
	// Get List of Items by applying explicit wait
	public List<WebElement> getItemListWait(By locator, int timeout) {
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		List<WebElement> itemList = wait.until(
				ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
		return itemList;
	}
	
}
