package utils;

import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import utils.ExecuteActions;
import searchPageObjects.SearchPageObjects;
import searchPageObjects.SearchResultsPageObjects;
import searchPageObjects.ItemDetailsPageObjects;

public class ExecuteActions {
	WebDriver driver;
	WaitTypes wt;
	GenericMethods gm;
	SearchPageObjects searchPage;
	SearchResultsPageObjects bsResults;
	ItemDetailsPageObjects itemDetails; 
	private String itemToClickText;
	public static final Logger log = LogManager.getLogger(ExecuteActions.class.getName());
	
	public ExecuteActions(WebDriver driver) {
		this.driver = driver;
		wt = new WaitTypes(driver);
		gm = new GenericMethods(driver);
		searchPage = new SearchPageObjects(driver);
		bsResults = new SearchResultsPageObjects(driver);
		itemDetails = new ItemDetailsPageObjects(driver);
	}
	
	public void enterSearchTerm(String searchString) {
		searchPage.clickSearchField();
		log.info("Clicked Search Field");
		searchPage.searchField().sendKeys(searchString);
		log.info("Entered Search String");
		searchPage.clickSearchButton();
		log.info("Clicked Search Button");
	}
	
	public void clickNewSearchButton() {
		bsResults.getNewSearchButton().click();
		log.info("Clicked New Search Button");
	}
	
	public void searchResultValidation(String searchString) {
		int size = bsResults.resutlsList().size();
		log.info("Search Results size: " +size);
		for (int i = 0; i < size; i++) {
			String text = bsResults.resutlsList().get(i).getText();
			String textLowerCase = text.toLowerCase();
			Assert.assertTrue(textLowerCase.contains(searchString));
			log.info("Assertion passed for String validation in Search Results");
		}
		
	}
	
	
	public void clickSearchResultItem() {
		Random random = new Random();
        WebElement randomitemToClick = bsResults.resutlTitles().get(random.nextInt(bsResults.resutlTitles().size()));
		itemToClickText = randomitemToClick.getText();
		log.info("Title on the results page of Item to be clicked: " +itemToClickText);
		randomitemToClick.click();
		log.info("Clicked on item from Search Results");
	}
	
	public void searchResultItemDetailsValidation() {
		clickSearchResultItem();
		Assert.assertEquals(itemDetails.clickedItemPageTitle(), itemToClickText); 
		log.info("Assertion passed for Title validation on Item details Page");
		driver.navigate().back();
	}
	

}
